
# [Section] Abstraction
# An abstract class can be considered as a blueprint for other classes. It allows you to create a set of methods that must be created within any child built from abstract class.

# A class which contains one or more abstract methods is called an abstract class.

# By default, Python does not provide abstract classes. Python comes with a module that provides the base for defining Abstract Base Classes (ABC) and that module name is ABC.
from abc import ABC, abstractmethod


class Person(ABC): #Person Class

    # Create an abstract method called -- that needs to be implemented by classes inherit --.
    @abstractmethod
    def getFullName(self):
    # This method will be craeted by the child class
        # The pass keyword denoted that the method doesn;t do anything
        pass
    # Create an abstract method called -- that needs to be implemented by classes inherit --.
    @abstractmethod
    def addRequest(self):
    # This method will be craeted by the child class
        # The pass keyword denoted that the method doesn;t do anything
        pass

    @abstractmethod
    def checkRequest(self):
        pass

    @abstractmethod
    def addUser(self):
        pass


class Employee(Person): #Employee Class
    def __init__(self, firstName, lastName, email, department):
        # attribute unique to Employee class
        self.__firstName = firstName
        self.__lastName = lastName
        self.__email = email
        self.__department = department

    #setters
    def setLastName(self, lastName):
        self.__lastName = lastName

    def setFirstName(self, firstName):
        self.__firstName = firstName

    def setEmail(self, email):
        self.__email = email

    def setDepartment(self, department):
        self.__department = department


    #getters
    def getFirstName(self):
        return self.__firstName

    def getLastName(self):
        return self.__lastName

    def getEmail(self):
        return self.__email

    def getDepartment(self):
        return self.__department

    def getFullName(self):
        return f"{self.__firstName} {self.__lastName}"



    def addRequest(self):
        return "Request has been added"

    def checkRequest(self):
        pass

    def addUser(self):
        pass

    def login(self):
        return f"{self.__email} has logged in"

    def logout(self):
        return f"{self.__email} has logged out"


class TeamLead(Person): #Team Lead Class
    def __init__(self, firstName, lastName, email, department):
        # attribute unique to TeamLead class
        self.__firstName = firstName
        self.__lastName = lastName
        self.__email = email
        self.__department = department
        self.__members = []

    #setters
    def setFirstName(self, firstName):
        self.__firstName = firstName

    def setLastName(self, lastName):
        self.__lastName = lastName

    def setEmail(self, email):
        self.__email = email

    def setDepartment(self, department):
        self.__department = department

    #getters
    def getFirstName(self):
        return self.__firstName

    def getLastName(self):
        return self.__lastName

    def getEmail(self):
        return self.__email

    def getDepartment(self):
        return self.__department

    def getMembers(self):
        return self.__members

    def getFullName(self):
        return f"{self.__firstName} {self.__lastName}"


    def addMember(self, member):
        self.__members.append(member)



    def addRequest(self):
        pass

    def checkRequest(self):
        return "Request has been checked"

    def addUser(self):
        pass

    def login(self):
        return f"{self.__email} has logged in"

    def logout(self):
        return f"{self.__email} has logged out"


#for Admin Class
class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        # attribute unique to Admin class
        self.__firstName = firstName
        self.__lastName = lastName
        self.__email = email
        self.__department = department

    # setters
    def setLastName(self, lastName):
        self.__lastName = lastName

    def setDepartment(self, department):
        self.__department = department

    def setEmail(self, email):
        self.__email = email

    def setFirstName(self, firstName):
        self.__firstName = firstName

    # getters
    def getFirstName(self):
        return self.__firstName

    def getLastName(self):
        return self.__lastName

    def getDepartment(self):
        return self.__department

    def getFullName(self):
        return f"{self.__firstName} {self.__lastName}"

    def getEmail(self):
        return self.__email

    #requesters
    def addRequest(self):
        return "Request has been added"

    def checkRequest(self):
        pass

    def addUser(self):
        return "User has been added"

    def login(self):
        return f"{self.__email} has logged in"

    def logout(self):
        return f"{self.__email} has logged out"


class Request: #Request Class
    def __init__(self, title, requester, date):
        # attribute unique to Request class
        self.title = title
        self.requester = requester
        self.date = date
        self.status = "open"

    #setter    
    def set_title(self, title):
        self.title = title
    
    def set_requester(self, requester):
        self.requester = requester

    def set_date(self, date):
        self.date = date
    
    def set_status(self, status):
        self.status = status

    #getters
    def get_title(self):
        return self.title

    def get_requester(self):
        return self.requester

    def get_date(self):
        return self.date
    
    def get_status(self):
        return self.status


    
    def closeRequest(self):
        self.status = "closed"
        return f"Request '{self.title}' has been closed."




# [SECTION] Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing") #upon creating/ initialization
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.getMembers():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())